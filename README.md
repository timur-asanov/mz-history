## MZ History

[![pipeline status](https://gitlab.com/timur-asanov/mz-history/badges/master/pipeline.svg)](https://gitlab.com/timur-asanov/mz-history/-/commits/master) [![coverage report](https://gitlab.com/timur-asanov/mz-history/badges/master/coverage.svg)](https://gitlab.com/timur-asanov/mz-history/-/commits/master)

A repository for the MZ History website.

See it in action: https://timur-asanov.gitlab.io/mz-history
