const path = require('path');
const fs = require('fs');

const appDirectory = fs.realpathSync(process.cwd());
const resolveApp = relativePath => path.resolve(appDirectory, relativePath);
const getPublicUrl = appPackageJson => require(appPackageJson).homepage;

module.exports = {
    appBuild: resolveApp('build'),
    appPublic: resolveApp('src/public'),
    appHtml: resolveApp('src/public/index.html'),
    appIndexJs: resolveApp('src/index.js'),
    appPackageJson: resolveApp('package.json'),
    appSrc: resolveApp('src'),
    publicUrl: getPublicUrl(resolveApp('package.json')),
    gitlabPublic: resolveApp('public')
};
