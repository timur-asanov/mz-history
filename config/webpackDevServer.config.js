const noopServiceWorkerMiddleware = require('react-dev-utils/noopServiceWorkerMiddleware');
const config = require('./webpack.config.dev');
const paths = require('./paths');

module.exports = function() {
    return {
        compress: true,
        clientLogLevel: 'none',
        contentBase: paths.appPublic,
        watchContentBase: true,
        hot: true,
        publicPath: config.output.publicPath,
        quiet: true,
        historyApiFallback: {
            disableDotRule: true
        },
        before(app) {
            app.use(noopServiceWorkerMiddleware());
        }
    };
};
