import os
import re
import json
from pathlib import Path

from model.Country import Country
from model.Winner import Winner
from model.OtherWinner import OtherWinner

DATA_FILES_DIR = "{}/data/raw".format(os.getcwd());

DESTINATION_FILE = "{}/src/data/countries.json".format(os.pardir)

COUNTRY_NAME_IN_PATH = "^.*\/(.*).txt$"

LINE_SEASON = "^season #(\d+):\s*(.*)\s*$"
LINE_CONTRIBUTORS = "^contributors:\s*(.*)\s*$"
LINE_OTHER_WINNERS = "^other winners:\s*(.*)\s*$"
LINE_LEAGUE_NAME = "^league name:\s*(.*)\s*$"
LINE_LEAGUE_START = "^league start:\s*(.*)\s*$"

def write_to_destination(countries):
    json_all = json.dumps(countries, default=serialize)

    output_file = open(DESTINATION_FILE, "w")
    output_file.write(json_all)
    output_file.close()

def serialize(obj):
    return obj.__dict__

def parse_line_league_start(lineMatch):
    league_start = lineMatch.group(1)

    if league_start is not None and league_start != "None":
        league_start = int(league_start)
    else:
        league_start = None

    return league_start

def parse_line_league_name(lineMatch):
    return lineMatch.group(1)

def parse_line_other_winners(lineMatch):
    other_winners = None

    if lineMatch.group(1) and lineMatch.group(1) != "None":
        other_winners = []

        other_winner_item_regex = re.compile('i\:(\d+).*?\"(.*?)\"')
        other_winner_matches = other_winner_item_regex.findall(lineMatch.group(1))

        for other_winner_match in other_winner_matches:
            other_winner = OtherWinner(int(other_winner_match[0]), other_winner_match[1])

            other_winners.append(other_winner)

    return other_winners

def parse_line_contributors(lineMatch):
    contributors = None

    if lineMatch.group(1) and lineMatch.group(1) != "None":
        contributor_item_regex = re.compile('\"(.*?)\"')
        contributors = contributor_item_regex.findall(lineMatch.group(1))

    return contributors

def parse_line_season(lineMatch):
    if lineMatch.group(2) and lineMatch.group(2) != "None":
        user_name = lineMatch.group(2)
    else:
        user_name = None

    winner = Winner(int(lineMatch.group(1)), user_name)

    return winner

def parse_data_file(data_file_location):
    country_name = None
    league_name = None
    league_start = None
    winners = None
    other_winners = None
    contributors = None

    data_file = Path(data_file_location)

    country_name_match = re.match(COUNTRY_NAME_IN_PATH, data_file_location)

    if country_name_match is not None:
        country_name = country_name_match.group(1).replace("_", " ")

        with open(data_file, encoding="utf8") as data_file_input:
            winners = []

            for line in data_file_input:
                if line.strip():
                    line = line.strip()

                    line_season_match = re.match(LINE_SEASON, line)
                    line_contributors_match = re.match(LINE_CONTRIBUTORS, line)
                    line_other_winners_match = re.match(LINE_OTHER_WINNERS, line)
                    line_league_name_match = re.match(LINE_LEAGUE_NAME, line)
                    line_league_start_match = re.match(LINE_LEAGUE_START, line)

                    if line_season_match is not None:
                        winner = parse_line_season(line_season_match);
                        winners.append(winner)
                    elif line_contributors_match is not None:
                        contributors = parse_line_contributors(line_contributors_match)
                    elif line_other_winners_match is not None:
                        other_winners = parse_line_other_winners(line_other_winners_match)
                    elif line_league_name_match is not None:
                        league_name = parse_line_league_name(line_league_name_match)
                    elif line_league_start_match is not None:
                        league_start = parse_line_league_start(line_league_start_match)

        data_file_input.close()

        country = Country(country_name, league_name, league_start, winners, other_winners, contributors)

        return country

def parse_data_files():
    countries = []

    for file_item in os.scandir(DATA_FILES_DIR):
        if file_item.is_file():
            country = parse_data_file(DATA_FILES_DIR + "/" + file_item.name)

            if country is not None:
                countries.append(country)

    return countries

def main():
    try:
        countries = parse_data_files()

        if countries is not None and len(countries) > 0:
            write_to_destination(countries)
    except Exception as e:
        print(e)

if __name__ == "__main__":
    main()
