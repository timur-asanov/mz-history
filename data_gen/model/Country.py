class Country(object):
    def __init__(self, name = None, league = None, league_start = None, winners = None, otherWinners = None, contributors = None, *args, **kwargs):
        self.name = name
        self.league = league
        self.league_start = league_start
        self.winners = winners
        self.otherWinners = otherWinners
        self.contributors = contributors

    def __repr__(self):
        return "<Country name:{} league:{} league_start:{} winners:{} otherWinners:{} contributors:{}>".format(self.name, self.league, self.league_start, self.winners, self.otherWinners, self.contributors)

    def __str__(self):
        return repr(self)
