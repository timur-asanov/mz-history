class OtherWinner(object):
    def __init__(self, season = None, teamName = None, *args, **kwargs):
        self.season = season
        self.teamName = teamName

    def __repr__(self):
        return "<OtherWinner season:{} teamName:{}>".format(self.season, self.teamName)

    def __str__(self):
        return repr(self)
