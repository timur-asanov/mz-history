class Winner(object):
    def __init__(self, season = None, username = None, *args, **kwargs):
        self.season = season
        self.username = username

    def __repr__(self):
        return "<Winner season:{} username:{}>".format(self.season, self.username)

    def __str__(self):
        return repr(self)
