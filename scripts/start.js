process.env.BABEL_ENV = 'development';
process.env.NODE_ENV = 'development';

/* eslint-disable no-console */

const chalk = require('chalk');
const webpack = require('webpack');
const WebpackDevServer = require('webpack-dev-server');

const {
    createCompiler,
    prepareUrls,
} = require('react-dev-utils/WebpackDevServerUtils');

const openBrowser = require('react-dev-utils/openBrowser');
const paths = require('../config/paths');
const config = require('../config/webpack.config.dev');
const createDevServerConfig = require('../config/webpackDevServer.config');

const port = 3000;
const host = 'localhost';

const protocol = 'http';
const appName = require(paths.appPackageJson).name;
const urls = prepareUrls(protocol, host, port);
const compiler = createCompiler(webpack, config, appName, urls, false);

const devServer = new WebpackDevServer(compiler, createDevServerConfig());

devServer.listen(port, host, err => {
    if (err) {
        return console.log(err);
    }

    console.log(chalk.cyan('Starting the development server...\n'));
    openBrowser(urls.localUrlForBrowser);
});

['SIGINT', 'SIGTERM'].forEach(function(sig) {
    process.on(sig, function() {
        devServer.close();
        process.exit();
    });
});
