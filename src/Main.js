import React from "react";
import { BrowserRouter as Router, Route, browserHistory } from "react-router-dom";
import { hot } from 'react-hot-loader';

import Sidebar from './components/common/Sidebar';
import Topbar from './components/common/Topbar';
import Footer from './components/common/Footer';

import Home from './views/Home';
import Countries from './views/Countries';
import Country from './views/Country';

class Main extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            sidebarIsOpen: true
        };

        this.handleSidebarToggle = this.handleSidebarToggle.bind(this);
    }

    handleSidebarToggle() {
        this.setState(prevState => ({
            sidebarIsOpen: !prevState.sidebarIsOpen
        }));
    }

    render() {
        return (
            <Router history={browserHistory} basename="/mz-history">
                <div className={(this.state.sidebarIsOpen === true ? 'sidebar_open' : 'sidebar_closed')}>
                    <Sidebar sidebarIsOpen={this.state.sidebarIsOpen} />
                    <Topbar sidebarToggleHandler={() => this.handleSidebarToggle} />

                    <section id="main">
                        <div className="negative_spacing">
                            <div className="col-md-12 col-sm-12 col-xs-12">
                                <div className="page_box">
                                    <Route exact path="/" component={Home} />
                                    <Route path="/countries" component={Countries} />
                                    <Route path="/country/:name" component={Country} />
                                </div>
                            </div>
                        </div>
                    </section>

                    <Footer />
                </div>
            </Router>
        );
    }
}

export default hot(module)(Main);
