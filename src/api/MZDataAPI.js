import countriesData from "../data/countries.json";
import properties from "../settings/properties.json";

import CountryData from "./model/CountryData";
import CountryStatistics from "./model/CountryStatistics";
import LongestRange from "./model/LongestRange";
import LongestStreak from "./model/LongestStreak";
import MostStreaks from "./model/MostStreaks";
import MostWins from "./model/MostWins";

import MZUtils from "../utils/util";

const currentSeason = properties.currentSeason;

function calculateCountryStatistics(country) {
    const leagueStart = country.league_start === null ? 1 : country.league_start;

    const winnersData = getWinnersData(leagueStart, country.winners);
    const uniqueWinners = generateUniqueWinnersData(winnersData.uniqueWinners);

    const percentageComplete = MZUtils.round(winnersData.countKnownWinners / (currentSeason - leagueStart) * 100, 2);

    const statistics = Object.assign(
        {},
        {
            league_start: leagueStart,
            percentageComplete,
            uniqueWinners,
            countUniqueWinners: uniqueWinners.length
        },
        winnersData
    );

    return statistics;
}

function getWinnersData(leagueStart, winners) {
    let countKnownWinners = 0;
    const uniqueWinners = [];

    for (let i = (currentSeason - 1); i >= leagueStart; i--) {
        const winner = findWinnerBySeason(winners, i);

        if (winner.username !== null) {
            countKnownWinners++;

            const uniqueWinnerIndex = findWinnerByUsername(uniqueWinners, winner.username);

            if (uniqueWinnerIndex !== -1) {
                const uniqueWinner = uniqueWinners[uniqueWinnerIndex];
                uniqueWinner.seasonsWon.push(i);

                uniqueWinners[uniqueWinnerIndex] = uniqueWinner;
            } else {
                uniqueWinners.push({ username: winner.username, seasonsWon: [i] });
            }
        }
    }

    return { countKnownWinners, uniqueWinners };
}

function generateUniqueWinnersData(uniqueWinners) {
    for (let i = 0; i < uniqueWinners.length; i++) {
        const uniqueWinner = uniqueWinners[i];
        const seasonsWon = uniqueWinner.seasonsWon.sort((a, b) => a - b);

        uniqueWinner.firstWin = seasonsWon[0];
        uniqueWinner.lastWin = seasonsWon[seasonsWon.length - 1];
        uniqueWinner.totalWins = seasonsWon.length;

        const streakData = calculateWinnerStreakData(seasonsWon);

        uniqueWinner.streaks = streakData.streaks;
        uniqueWinner.maxStreak = streakData.maxStreak;

        uniqueWinners[i] = uniqueWinner;
    }

    return uniqueWinners;
}

function calculateWinnerStreakData(seasonsWon) {
    const allStreaks = [];
    let maxStreak = { start: 0, end: 1 };

    if (seasonsWon.length > 1) {
        let prev = seasonsWon[0];
        let tempStreak = { start: prev, end: prev };

        for (let z = 1; z < seasonsWon.length; z++) {
            if (seasonsWon[z] - 1 === prev) {
                tempStreak.end = seasonsWon[z];
            } else {
                maxStreak = calculateMaxStreak(maxStreak, tempStreak);

                allStreaks.push(tempStreak);
                tempStreak = { start: seasonsWon[z], end: seasonsWon[z] };
            }

            prev = seasonsWon[z];
        }

        maxStreak = calculateMaxStreak(maxStreak, tempStreak);

        allStreaks.push(tempStreak);
    }

    const streaks = [];

    for (let y = 0; y < allStreaks.length; y++) {
        const streak = allStreaks[y];

        if (streak.end - streak.start !== 0) {
            streaks.push(streak);
        }
    }

    return { streaks: streaks, maxStreak: maxStreak };
}

function calculateMaxStreak(currentMax, streak) {
    if ((1 + streak.end - streak.start) > (1 + currentMax.end - currentMax.start)) {
        return streak;
    }

    return currentMax;
}

function findWinnerBySeason(winners, season) {
    return winners.find(item => item.season === season);
}

function findWinnerByUsername(winners, username) {
    return winners.findIndex(item => item.username === username);
}

function findChunkByLetter(countriesChunks, letter) {
    return countriesChunks.findIndex(item => item.letter === letter);
}

function loadData() {
    const countries = [];

    for (let i = 0; i < countriesData.length; i++) {
        const country = new CountryData(countriesData[i]);

        countries.push(country);
    }

    return countries;
}

function sortByNumberOfWinners(countriesStatistics) {
    return countriesStatistics.sort((a, b) => a.countUniqueWinners - b.countUniqueWinners);
}

function sortByPercentageComplete(countriesStatistics) {
    return countriesStatistics.sort((a, b) => a.percentageComplete - b.percentageComplete);
}

function findLongestRangeInCountry(countryStatistics) {
    let longestRange = new LongestRange(null, null, 0, 0);

    for (let j = 0; j < countryStatistics.uniqueWinners.length; j++) {
        const uniqueWinner = countryStatistics.uniqueWinners[j];

        if (uniqueWinner.lastWin - uniqueWinner.firstWin > longestRange.range) {
            longestRange = new LongestRange(uniqueWinner.username, countryStatistics.name, uniqueWinner.firstWin, uniqueWinner.lastWin);
        }
    }

    return longestRange;
}

function findLongestStreakInCountry(countryStatistics) {
    let longestStreak = new LongestStreak(null, null, { start: 0, end: 1 });

    for (let j = 0; j < countryStatistics.uniqueWinners.length; j++) {
        const uniqueWinner = countryStatistics.uniqueWinners[j];

        if ((1 + uniqueWinner.maxStreak.end - uniqueWinner.maxStreak.start) > (1 + longestStreak.streak.end - longestStreak.streak.start)) {
            longestStreak = new LongestStreak(uniqueWinner.username, countryStatistics.name, uniqueWinner.maxStreak);
        }
    }

    return longestStreak;
}

function findMostStreaksInCountry(countryStatistics) {
    let mostStreaks = new MostStreaks(null, null, null);

    for (let j = 0; j < countryStatistics.uniqueWinners.length; j++) {
        const uniqueWinner = countryStatistics.uniqueWinners[j];

        if (uniqueWinner.streaks.length > mostStreaks.numStreaks) {
            mostStreaks = new MostStreaks(uniqueWinner.username, countryStatistics.name, uniqueWinner.streaks);
        }
    }

    return mostStreaks;
}

function findMostWinsInCountry(countryStatistics) {
    const userWithMostWins = countryStatistics.uniqueWinners.sort((a, b) => a.totalWins - b.totalWins || a.lastWin - b.lastWin).reverse()[0];

    return new MostWins(userWithMostWins.username, countryStatistics.name, userWithMostWins.totalWins);
}

class MZDataAPI {
    static getCountriesStatistics() {
        const countriesStatistics = [];
        const countries = loadData();

        for (let i = 0; i < countries.length; i++) {
            const country = countries[i];
            const statistics = calculateCountryStatistics(country);
            const countryStatistics = new CountryStatistics(country, statistics);

            countriesStatistics.push(countryStatistics);
        }

        return countriesStatistics;
    }

    static getCountryStatistics(countryName) {
        const countries = loadData();
        const country = countries.find(country => country.name === countryName);

        const statistics = calculateCountryStatistics(country);

        return new CountryStatistics(country, statistics);
    }

    static getMostCompletedCountries(numOffTheTop) {
        const countriesStatistics = this.getCountriesStatistics();

        return sortByPercentageComplete(countriesStatistics).slice(-numOffTheTop).reverse();
    }

    static getLeastCompletedCountries(numOffTheBottom) {
        const countriesStatistics = this.getCountriesStatistics();

        return sortByPercentageComplete(countriesStatistics).slice(0, numOffTheBottom);
    }

    static getCountriesWithMostWinners(numOffTheTop) {
        const countriesStatistics = this.getCountriesStatistics();

        return sortByNumberOfWinners(countriesStatistics).slice(-numOffTheTop).reverse();
    }

    static getCountriesWithLeastWinners(numOffTheBottom) {
        const countriesStatistics = this.getCountriesStatistics();

        return sortByNumberOfWinners(countriesStatistics).slice(0, numOffTheBottom);
    }

    static divideCountries() {
        const countries = loadData();
        let countriesChunks = [];

        for(let i = 0; i < countries.length; i++) {
            const country = countries[i];
            const countryIndex = findChunkByLetter(countriesChunks, country.name.charAt(0));

            if (countryIndex !== -1) {
                const countriesChunk = countriesChunks[countryIndex];
                countriesChunk.countries.push(country.name);

                countriesChunks[countryIndex] = countriesChunk;
            } else {
                countriesChunks.push({ letter: country.name.charAt(0), countries: [country.name] });
            }
        }

        countriesChunks.sort(function(a, b) {
            return (a.letter < b.letter) ? -1 : (a.letter > b.letter) ? 1 : 0;
        });

        return countriesChunks;
    }

    static getLongestRangeInCountry(countryName) {
        const countryStatistics = this.getCountryStatistics(countryName);

        return findLongestRangeInCountry(countryStatistics);
    }

    static getLongestStreakInCountry(countryName) {
        const countryStatistics = this.getCountryStatistics(countryName);

        return findLongestStreakInCountry(countryStatistics);
    }

    static getMostStreaksInCountry(countryName) {
        const countryStatistics = this.getCountryStatistics(countryName);

        return findMostStreaksInCountry(countryStatistics);
    }

    static getLongestRange() {
        const countriesStatistics = this.getCountriesStatistics();
        let longestRange = new LongestRange(null, null, 0, 0);

        for (let j = 0; j < countriesStatistics.length; j++) {
            const tempLongestRange = findLongestRangeInCountry(countriesStatistics[j]);

            if (tempLongestRange.lastWin - tempLongestRange.firstWin > longestRange.range) {
                longestRange = tempLongestRange;
            }
        }

        return longestRange;
    }

    static getLongestStreak() {
        const countriesStatistics = this.getCountriesStatistics();
        let longestStreak = new LongestStreak(null, null, { start: 0, end: 1 });

        for (let j = 0; j < countriesStatistics.length; j++) {
            const tempLongestStreak = findLongestStreakInCountry(countriesStatistics[j]);

            if ((1 + tempLongestStreak.streak.end - tempLongestStreak.streak.start) > (1 + longestStreak.streak.end - longestStreak.streak.start)) {
                longestStreak = tempLongestStreak;
            }
        }

        return longestStreak;
    }

    static getMostStreaks() {
        const countriesStatistics = this.getCountriesStatistics();
        let mostStreaks = new MostStreaks(null, null, null);

        for (let j = 0; j < countriesStatistics.length; j++) {
            const tempMostStreaks = findMostStreaksInCountry(countriesStatistics[j]);

            if (tempMostStreaks.streaks.length > mostStreaks.numStreaks) {
                mostStreaks = tempMostStreaks;
            }
        }

        return mostStreaks;
    }

    static getMostWinsInCountry(countryName) {
        const countryStatistics = this.getCountryStatistics(countryName);

        return findMostWinsInCountry(countryStatistics);
    }

    static getMostWins(numOffTheTop) {
        const countriesStatistics = this.getCountriesStatistics();
        const allMostWins = [];

        for (let j = 0; j < countriesStatistics.length; j++) {
            allMostWins.push(findMostWinsInCountry(countriesStatistics[j]));
        }

        return allMostWins.sort((a, b) => a.count - b.count).slice(-numOffTheTop).reverse();
    }
}

export default MZDataAPI;
