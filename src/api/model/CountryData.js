class CountryData {
    constructor(data = {}) {
        Object.assign(this, data);
    }
}

export default CountryData;
