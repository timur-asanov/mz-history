class CountryStatistics {
    constructor(countryData = {}, statistics = {}) {
        Object.assign(this, countryData, statistics);
    }
}

export default CountryStatistics;
