import Statistic from './Statistic';

class LongestRange extends Statistic {
    constructor(username, country, firstWin, lastWin) {
        super(username, country);

        this.firstWin = firstWin;
        this.lastWin = lastWin;
        this.range = lastWin - firstWin;
    }

    getStatisticPrint() {
        return "from " + this.firstWin + " to " + this.lastWin + ": " + this.range + " season(s)";
    }
}

export default LongestRange;
