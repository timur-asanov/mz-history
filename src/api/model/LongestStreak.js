import Statistic from './Statistic';

class LongestStreak extends Statistic {
    constructor(username, country, streak) {
        super(username, country);

        this.streak = streak;
        this.firstWin = streak.start;
        this.lastWin = streak.end;
    }

    getStatisticPrint() {
        return "from " + this.firstWin + " to " + this.lastWin + ": " + (this.lastWin - this.firstWin + 1) + " season(s)";
    }
}

export default LongestStreak;
