import Statistic from './Statistic';

class MostStreaks extends Statistic {
    constructor(username, country, streaks) {
        super(username, country);

        this.streaks = streaks;
        this.numStreaks = streaks === undefined || streaks == null ? 0 : streaks.length;
    }

    getStatisticPrint() {
        return "with " + this.numStreaks + " streak(s)";
    }
}

export default MostStreaks;
