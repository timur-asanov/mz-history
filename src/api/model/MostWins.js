import Statistic from './Statistic';

class MostWins extends Statistic {
    constructor(username, country, count) {
        super(username, country);

        this.count = count;
    }

    getStatisticPrint() {
        return "with " + this.count + " win(s)";
    }
}

export default MostWins;
