class Statistic {
    constructor(username, country) {
        this.username = username;
        this.country = country;
    }
}

export default Statistic;
