import React from 'react';
import PropTypes from 'prop-types';

const ContributorList = ({contributors}) => {
    return (
        <div className="page_box">
            <div className="page_box_title">
                <h3>Contributors</h3>
                <p style={{clear: 'both'}}>
                    Below is a list of users that have helped with finding the information above.
                </p>
            </div>

            <div className="page_box_content">
                <table>
                    <tbody>
                        {contributors.map((contributor) => (
                            <tr key={contributor}>
                                <td>{contributor}</td>
                            </tr>
                        ))}
                    </tbody>
                </table>
            </div>
        </div>
    );
};

ContributorList.propTypes = {
    contributors: PropTypes.array.isRequired
};

export default ContributorList;
