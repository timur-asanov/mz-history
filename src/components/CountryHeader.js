import React from "react";
import PropTypes from 'prop-types';

import styles from './country-header-styles.module.css';

const CountryHeader = ({leagueStart, countUniqueWinners, percentageComplete}) => {
    return (
        <div style={{margin: '10px 0'}}>
            <div className={'col-md-4 col-sm-4 col-xs-12' + ' ' + styles.tile}>
                <span>Season Started</span>
                <h2>{leagueStart}</h2>
            </div>
            <div className={'col-md-4 col-sm-4 col-xs-12' + ' ' + styles.tile}>
                <span>Unique Winners</span>
                <h2>{countUniqueWinners}</h2>
            </div>
            <div className={'col-md-4 col-sm-4 col-xs-12' + ' ' + styles.tile}>
                <span>Completion</span>
                <h2>{percentageComplete}</h2>
            </div>
        </div>
    );
};

CountryHeader.propTypes = {
    leagueStart: PropTypes.number.isRequired,
    countUniqueWinners: PropTypes.number.isRequired,
    percentageComplete: PropTypes.number.isRequired
};

export default CountryHeader;
