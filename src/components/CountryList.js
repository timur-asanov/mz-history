import React from 'react';
import PropTypes from 'prop-types';

import CountryListItem from './CountryListItem';

const CountryList = ({title, countryList}) => {
    return (
        <div className="page_box">
            <div className="page_box_title">
                <h3>{title}</h3>
            </div>

            <div className="page_box_content">
                <table>
                    <tbody>
                        {
                            countryList.map(function(country, index) {
                                return (
                                    <CountryListItem key={country.name} position={index + 1} country={country} />
                                );
                            })
                        }
                    </tbody>
                </table>
            </div>
        </div>
    );
};

CountryList.propTypes = {
    title: PropTypes.string.isRequired,
    countryList: PropTypes.array.isRequired
};

export default CountryList;
