import React from 'react';
import PropTypes from 'prop-types';
import { Link } from "react-router-dom";

const CountryListItem = ({position, country}) => {
    return (
        <tr>
            <th>{position}</th>
            <td><Link to={`/country/${encodeURIComponent(country.name)}`}>{country.name}</Link>{' - '}{country.statistic}</td>
        </tr>
    );
};

CountryListItem.propTypes = {
    position: PropTypes.number.isRequired,
    country: PropTypes.object.isRequired
};

export default CountryListItem;
