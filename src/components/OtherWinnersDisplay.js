import React from 'react';
import PropTypes from 'prop-types';

const OtherWinnersDisplay = ({otherWinners}) => {
    return (
        <div className="page_box">
            <div className="page_box_title">
                <h3>Unidentified Winners</h3>
                <p style={{clear: 'both'}}>
                    Below is a list of known team names used by the winners in this league. Unfortunately, we do not know their username(s). Do you know their username(s)? Contact me! (user: thecollector)
                </p>
            </div>

            <div className="page_box_content">
                <table>
                    <tbody>
                        {otherWinners.map((otherWinner) => (
                            <tr key={otherWinner.season}>
                                <td>{'season #'}{otherWinner.season}</td>
                                <td>{otherWinner.teamName == null ? "\u002D" : otherWinner.teamName}</td>
                            </tr>
                        ))}
                    </tbody>
                </table>
            </div>
        </div>
    );
};

OtherWinnersDisplay.propTypes = {
    otherWinners: PropTypes.array.isRequired
};

export default OtherWinnersDisplay;
