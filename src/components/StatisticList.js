import React from 'react';
import PropTypes from 'prop-types';

import StatisticListItem from "./StatisticListItem";

const StatisticList = ({items, showCountry}) => {
    return (
        <div style={{clear: 'both'}}>
            <h5>General Statistics</h5>
            {
                items.map(item => {
                    return (
                        <StatisticListItem key={item.title} title={item.title} statistic={item.statistic} showCountry={showCountry} />
                    );
                })
            }
        </div>
    );
};

StatisticList.propTypes = {
    items: PropTypes.array.isRequired,
    showCountry: PropTypes.bool
};

StatisticList.defaultProps = {
    showCountry: false
};


export default StatisticList;
