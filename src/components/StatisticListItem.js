import React from 'react';
import PropTypes from 'prop-types';

const StatisticItem = ({title, statistic, showCountry}) => {
    return (
        <p>
            {title}:
            {' '}<strong>{statistic.username}</strong>{' '}
            {showCountry === true ? '(' + statistic.country + ')' : ''}
            {' '}{statistic.getStatisticPrint()}
        </p>
    );
};

StatisticItem.propTypes = {
    title: PropTypes.string.isRequired,
    statistic: PropTypes.object.isRequired,
    showCountry: PropTypes.bool
};

StatisticItem.defaultProps = {
    showCountry: false
};

export default StatisticItem;
