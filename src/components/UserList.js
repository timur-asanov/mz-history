import React from 'react';
import PropTypes from 'prop-types';

import UserListItem from './UserListItem';

const UserList = ({title, userList}) => {
    return (
        <div className="page_box">
            <div className="page_box_title">
                <h3>{title}</h3>
            </div>

            <div className="page_box_content">
                <table>
                    <tbody>
                        {
                            userList.map(function(user, index) {
                                return (
                                    <UserListItem key={user.name} position={index + 1} user={user} />
                                );
                            })
                        }
                    </tbody>
                </table>
            </div>
        </div>
    );
};

UserList.propTypes = {
    title: PropTypes.string.isRequired,
    userList: PropTypes.array.isRequired
};

export default UserList;
