import React from 'react';
import PropTypes from 'prop-types';

const UserListItem = ({position, user}) => {
    return (
        <tr>
            <th>{position}</th>
            <td>{user.name}{' (' + user.countryName + ') '}{' - '}{user.statistic}</td>
        </tr>
    );
};

UserListItem.propTypes = {
    position: PropTypes.number.isRequired,
    user: PropTypes.object.isRequired
};

export default UserListItem;
