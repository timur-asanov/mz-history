import React from "react";
import PropTypes from 'prop-types';

const WinnersDisplay = ({countryWinners}) => {

    const chunkSize = 10;

    const winnerChunks = countryWinners.reduce((all, one, i) => {
        const ch = Math.floor(i / chunkSize);
        all[ch] = [].concat((all[ch] || []), one);

        return all;
    }, []);

    return (
        <div>
            {winnerChunks.map((winnerChunk, i) => (
                <div key={i} className="col-md-4 col-sm-4 col-xs-12">
                    <div className="page_box">
                        <div className="page_box_content">
                            <table>
                                <tbody>
                                    {winnerChunk.map((winner) => (
                                        <tr key={winner.season}>
                                            <td>{'season #'}{winner.season}</td>
                                            <td>{winner.username == null ? "" : winner.username}</td>
                                        </tr>
                                    ))}
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            ))}
        </div>
    );
};

WinnersDisplay.propTypes = {
    countryWinners: PropTypes.array.isRequired
};

export default WinnersDisplay;
