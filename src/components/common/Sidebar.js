import React from "react";
import PropTypes from 'prop-types';
import { NavLink } from "react-router-dom";

import styles from './sidebar-styles.module.css';

import MZUtils from "../../utils/util";

const Sidebar = ({sidebarIsOpen}) => {
    const selectedStyleOpen = {
        borderRight: '5px solid #1ABB9C',
        textShadow: 'rgba(0, 0, 0, 0.25) 0 -1px 0',
        background: 'linear-gradient(#334556, #2C4257), #2A3F54',
        boxShadow: 'rgba(0, 0, 0, 0.25) 0 1px 0, inset rgba(255, 255, 255, 0.16) 0 1px 0'
    };

    const selectedStyleClosed = Object.assign({}, selectedStyleOpen, {color: '#1ABB9C'});

    let navTitleStyle;
    let mainMenuStyle;
    let selectedStyle;
    let rootStyle;

    if (sidebarIsOpen === true) {
        navTitleStyle = styles.navTitleOpen;
        mainMenuStyle = styles.mainMenuOpen;
        selectedStyle = selectedStyleOpen;
        rootStyle = styles.rootOpen;
    } else {
        navTitleStyle = styles.navTitleClosed;
        mainMenuStyle = styles.mainMenuClosed;
        selectedStyle = selectedStyleClosed;
        rootStyle = styles.rootClosed;
    }

    const countryName = MZUtils.getCountryNameFromPath(window.location.pathname);

    return (
        <aside className={rootStyle}>
            <div className={navTitleStyle}>
                <NavLink exact to="/" className={styles.siteTitle}><i className="fa fa-database"></i> <span>MZ History</span></NavLink>
            </div>

            <nav className={mainMenuStyle}>
                <ul>
                    <li>
                        <NavLink exact to="/" activeStyle={selectedStyle}><i className={styles.mainMenuFA + ' ' + 'fa-home'}></i> Home</NavLink>
                    </li>
                    <li>
                        <NavLink to="/countries" activeStyle={selectedStyle}><i className={styles.mainMenuFA + ' ' + 'fa-globe'}></i> Countries</NavLink>
                    </li>
                    {
                        countryName !== undefined && countryName != null
                        ?   <li>
                                <a style={selectedStyle} disabled><i className={styles.mainMenuFA + ' ' + 'fa-flag'}></i> {countryName}</a>
                            </li>
                        : ''
                    }
                </ul>
            </nav>
        </aside>
    );
};

Sidebar.propTypes = {
    sidebarIsOpen: PropTypes.bool.isRequired
};

export default Sidebar;
