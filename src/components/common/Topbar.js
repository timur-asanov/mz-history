import React from "react";
import PropTypes from 'prop-types';

const Topbar = ({sidebarToggleHandler}) => {
    return (
        <section id="top_bar">
            <div id="toggle">
                <a onClick={sidebarToggleHandler()}><i className="fa fa-bars"></i></a>
            </div>
        </section>
    );
};

Topbar.propTypes = {
    sidebarToggleHandler: PropTypes.func.isRequired
};

export default Topbar;
