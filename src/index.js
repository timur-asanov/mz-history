import React from "react";
import ReactDOM from "react-dom";
import Main from "./Main";

import 'font-awesome/css/font-awesome.min.css';
import './css/grid.css';
import './css/styles.css';

ReactDOM.render(<Main/>, document.getElementById("app"));
