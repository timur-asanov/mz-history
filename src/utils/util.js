class MZUtils {
    static round(item, precision) {
        return + (Math.round(item + "e+" + precision)  + "e-" + precision);
    }

    static mapNumberOfWinnersList(countryList) {
        return countryList.map(country => {
            return {
                name: country.name,
                statistic: country.countUniqueWinners + ' user(s)'
            };
        });
    }

    static mapPercentageList(countryList) {
        return countryList.map(country => {
            return {
                name: country.name,
                statistic: country.percentageComplete + ' \u0025'
            };
        });
    }

    static mapUserStatistic(userList, statisticName) {
        return userList.map(user => {
            return {
                name: user.username,
                countryName: user.country,
                statistic: user.count + ' ' + statisticName
            };
        });
    }

    static getCountryNameFromPath(path) {
        const countryPathRegex = /^.+mz-history\/country\/(.+)$/;
        const matchedCountryPage = countryPathRegex.exec(path);
        let countryName;

        if (matchedCountryPage !== undefined && matchedCountryPage != null && matchedCountryPage.length === 2) {
            countryName = decodeURIComponent(matchedCountryPage[1]);
        }

        return countryName;
    }
}

export default MZUtils;
