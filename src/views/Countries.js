import React from "react";
import { Link } from "react-router-dom";

import MZDataAPI from "../api/MZDataAPI";

class Countries extends React.Component {
    render() {
        const countriesChunks = MZDataAPI.divideCountries();

        return (
            <div>
                <div className="page_box_title">
                    <h2>Countries</h2>
                </div>

                <div className="page_box_content">
                    {countriesChunks.map((countriesChunk, i) => (
                        <div key={i}>
                            <div className="col-md-4 col-sm-4 col-xs-12">
                                <div className="page_box">
                                    <div className="page_box_title">
                                        <h3>{countriesChunk.letter}</h3>
                                    </div>

                                    <div className="page_box_content">
                                        <table>
                                            <tbody>
                                                {countriesChunk.countries.map((countryName) => (
                                                    <tr key={countryName}>
                                                        <td><Link to={`/country/${encodeURIComponent(countryName)}`}>{countryName}</Link></td>
                                                    </tr>
                                                ))}
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>

                            <div style={ (i + 1) % 3 === 0 ? {clear: 'both'} : {}}></div>
                        </div>
                    ))}
                </div>
            </div>
        );
    }
}

export default Countries;
