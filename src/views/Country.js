import React from "react";

import MZDataAPI from "../api/MZDataAPI";

import WinnersDisplay from "../components/WinnersDisplay";
import StatisticList from "../components/StatisticList";
import ContributorList from "../components/ContributorList";
import OtherWinnersDisplay from "../components/OtherWinnersDisplay";
import CountryHeader from "../components/CountryHeader";

class Country extends React.Component {
    render() {
        const { match } = this.props;

        const countryName = match.params.name;

        const countryStatistics = MZDataAPI.getCountryStatistics(countryName);

        const longestRangeInTestCountry = MZDataAPI.getLongestRangeInCountry(countryName);
        const longestStreakInTestCountry = MZDataAPI.getLongestStreakInCountry(countryName);
        const mostStreaksInTestCountry = MZDataAPI.getMostStreaksInCountry(countryName);
        const mostWinsInTestCountry = MZDataAPI.getMostWinsInCountry(countryName);

        const statisticItems = new Array(
            { title: "User with most wins", statistic: mostWinsInTestCountry },
            { title: "User with most seasons between the first win and the last win", statistic: longestRangeInTestCountry },
            { title: "User with the most consecutive wins", statistic: longestStreakInTestCountry },
            { title: "User with the most consecutive wins streaks", statistic: mostStreaksInTestCountry }
        );

        return (
            <div>
                <div className="page_box_title">
                    <h2>{countryStatistics.name} - {countryStatistics.league}</h2>
                </div>

                <div className="page_box_content">

                    {/*TODO league start is never null because it is set to 1 in [somewhere] so what needs to happen is to not set it to one unless it is 1, and then use null check in calculations*/}
                    <CountryHeader
                        leagueStart = {countryStatistics.league_start === null ? "?" : countryStatistics.league_start}
                        countUniqueWinners = {countryStatistics.countUniqueWinners}
                        percentageComplete = {countryStatistics.percentageComplete} />

                    <StatisticList items={statisticItems} />

                    <div style={{clear: 'both'}}></div>

                    <WinnersDisplay countryWinners={countryStatistics.winners} />

                    <div style={{clear: 'both'}}></div>

                    {countryStatistics.contributors
                        ?   <div className="col-md-6 col-sm-6 col-xs-12">
                                <ContributorList contributors={countryStatistics.contributors} />
                            </div>
                        : ''
                    }

                    {countryStatistics.otherWinners
                        ?   <div className="col-md-6 col-sm-6 col-xs-12">
                                <OtherWinnersDisplay otherWinners={countryStatistics.otherWinners} />
                            </div>
                        : ''
                    }
                </div>
            </div>
        );
    }
}

export default Country;
