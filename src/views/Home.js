import React from "react";

import MZDataAPI from "../api/MZDataAPI";
import MZUtils from "../utils/util";

import CountryList from '../components/CountryList';
import UserList from '../components/UserList';
import StatisticList from "../components/StatisticList";

import properties from "../settings/properties.json";

class Home extends React.Component {
    render() {
        const longestRange = MZDataAPI.getLongestRange();
        const longestStreak = MZDataAPI.getLongestStreak();
        const mostStreaks = MZDataAPI.getMostStreaks();

        const statisticItems = new Array(
            { title: "User with most seasons between the first win and the last win", statistic: longestRange, showCountry: true },
            { title: "User with the most consecutive wins", statistic: longestStreak, showCountry: true },
            { title: "User with the most consecutive wins streaks", statistic: mostStreaks, showCountry: true }
        );

        let top10winners = MZDataAPI.getCountriesWithMostWinners(10);
        let bottom10winners = MZDataAPI.getCountriesWithLeastWinners(10);

        top10winners = MZUtils.mapNumberOfWinnersList(top10winners);
        bottom10winners = MZUtils.mapNumberOfWinnersList(bottom10winners);

        let top10complete = MZDataAPI.getMostCompletedCountries(10);
        let bottom10complete = MZDataAPI.getLeastCompletedCountries(10);

        top10complete = MZUtils.mapPercentageList(top10complete);
        bottom10complete = MZUtils.mapPercentageList(bottom10complete);

        let top10users = MZDataAPI.getMostWins(10);
        top10users = MZUtils.mapUserStatistic(top10users, "win(s)");

        return (
            <div>
                <div className="page_box_title">
                    <h2>Home</h2>
                </div>

                <div className="page_box_content">

                    <p>
                        This website contains historical data about winners of top leagues in ManagerZone. The data collection started in season 38 so most of the early seasons' winners, in most countries, are not known. The data was collected by checking every user in the top leagues and divisions 1.1, 1.2 and 1.3.
                    </p>

                    <p>
                        If you have any information about the winners for any top league in any country or if you know when any country has been officially added to ManagerZone or you have any suggestions, then please let me know in my guestbook (user: thecollector).
                    </p>

                    <p>The current season is #{properties.currentSeason}</p>

                    <p><strong>NOTE: </strong><em>If you would like for your username to be removed from this website, let me know.</em></p>

                    <div className="col-md-6 col-sm-6 col-xs-12">
                        <CountryList
                            title="Top 10 Completed Countries"
                            countryList={top10complete} />
                    </div>

                    <div className="col-md-6 col-sm-6 col-xs-12">
                        <CountryList
                            title="Bottom 10 Completed Countries"
                            countryList={bottom10complete} />
                    </div>

                    <div className="col-md-6 col-sm-6 col-xs-12">
                        <CountryList
                            title="Top 10 Unique Winners"
                            countryList={top10winners} />
                    </div>

                    <div className="col-md-6 col-sm-6 col-xs-12">
                        <CountryList
                            title="Bottom 10 Unique Winners"
                            countryList={bottom10winners} />
                    </div>

                    <div className="col-md-6 col-sm-6 col-xs-12">
                        <UserList
                            title="Top 10 Most Wins"
                            userList={top10users} />
                    </div>

                    <StatisticList items={statisticItems} showCountry={true} />
                </div>
            </div>
        );
    }
}

export default Home;
